import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;

public class BurgerOrder {
    private JPanel root;
    private JLabel Ordered;
    private JButton CheckOut;
    private JButton BigMac;
    private JButton doublecheese;
    private JButton cheese;
    private JButton teriyaki;
    private JButton fireofish;
    private JButton gurakoro;
    private JLabel CheesePrice;
    private JLabel DoubleCheesePrice;
    private JLabel BigMacProce;
    private JLabel TeriyakiPrice;
    private JLabel GurakoroPrice;
    private JLabel FireoFishPrice;
    private JTextPane OrderedItemsList;
    private JLabel Total;
    private JButton Coupon;
    private double TotalPrice=0;
    String[] coupon={"NO","Ultra(10%off)","Super(20%off)","Deluxe(30%off)"};
    double[] setcoupon={1,0.9,0.8,0.7};

    void order(String food,int Price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0){//Yes
            String currentText=OrderedItemsList.getText();
            OrderedItemsList.setText(currentText+food+" "+Price+" dollars\n");
            TotalPrice+=Price;

            Total.setText("Total    "+TotalPrice+"   dollars");
            JOptionPane.showMessageDialog(null,
                    "Thank you for Ordering "+food+"! It will be served as soon as possible.");
        }
    }

void coupon(double couponN){
    TotalPrice = TotalPrice * couponN;
    Total.setText("Total   " + String.format("%.1f",TotalPrice) + "dollars");
}

    public BurgerOrder() {
        cheese.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("CheeseBurger", 34);
            }
        });
        cheese.setIcon(new ImageIcon(this.getClass().getResource("humburger2/Cheese.png")
        ));


        doublecheese.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("DoubleCheeseBurger", 33);
            }
        });
        doublecheese.setIcon(new ImageIcon(this.getClass().getResource("humburger2/DoubleCheese.png")
        ));


        BigMac.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("BigMac", 39);
            }
        });
        BigMac.setIcon(new ImageIcon(this.getClass().getResource("humburger2/BigMac.png")
        ));


        teriyaki.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("TeriyakiBurger", 37);
            }
        });
        teriyaki.setIcon(new ImageIcon(Objects.requireNonNull(this.getClass().getResource("humburger2/Teriyaki.png"))
        ));


        fireofish.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("FireoFish", 35);
            }
        });
        fireofish.setIcon(new ImageIcon(this.getClass().getResource("humburger2/Fireo.png")
        ));


        gurakoro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gurakoro", 36);
            }
        });
        gurakoro.setIcon(new ImageIcon(this.getClass().getResource("humburger2/Gurakoro.png")
        ));


        CheckOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to check out ?",
                        "Check Out Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {//Yes
                    JOptionPane.showMessageDialog(null,
                            "Thank you.Your payment is "+String.format("%.1f",TotalPrice)+"  dollars.");
                        TotalPrice=0;
                        Total.setText("Total   " + TotalPrice + "dollars");
                        OrderedItemsList.setText("");
                }

            }
        });

        Coupon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int havecoupon = JOptionPane.showConfirmDialog(null,
                        "Would you have any coupons ?",
                        "Coupons",
                        JOptionPane.YES_NO_OPTION);
                if (havecoupon == 0) {
                    int selectcoupon = JOptionPane.showOptionDialog(null,
                            "Please choose coupone you have",
                            "Select coupon",
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE,
                            null,
                            coupon,
                            setcoupon[0]
                    );
                    if(selectcoupon==0){
                        coupon(1.0);
                    }
                    else if (selectcoupon == 1) {
                        coupon(0.9);
                    }
                    else if(selectcoupon==2){
                        coupon(0.8);
                    }
                    else if(selectcoupon==3){
                        coupon(0.7);
                    }
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("BurgerOrder");
        frame.setContentPane(new BurgerOrder().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
